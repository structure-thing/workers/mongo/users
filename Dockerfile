FROM python:3.11.6-alpine3.18

RUN apk add build-base git
RUN mkdir /app
WORKDIR /app

COPY requirements.txt ./
RUN pip install -r requirements.txt

RUN pip uninstall -y bson pymongo
RUN pip install pymongo

COPY app.py ./

CMD python app.py
