import asyncio
import pymongo
import bson
import re
import json
import string
import random
import secrets
import nats
import nats_json_rpc

async def main():
    connection = await nats.connect('nats://nats')

    rpc = nats_json_rpc.NatsJsonRPC(connection, 'structure.rpc.')
    rpc.codecs.append(nats_json_rpc.MongoIdCodec())

    mongo = pymongo.MongoClient('mongo')
    database = mongo['structure']
    collection = database['users']

    def get_random_string(length):
        letters = string.ascii_lowercase
        return ''.join(random.choice(letters) for i in range(length))

    @rpc.rpc_register
    async def users_create(*, user: dict):
        result = collection.insert_one(user)
        user_inserted = collection.find_one(result.inserted_id)
        return user_inserted

    @rpc.rpc_register
    async def users_create_invitation(node: dict, type: string, label: string, permissions: dict):
        if type == 'invitation':
            document = {
                'type': 'invitation',
                'label': label,
                'username': secrets.token_hex(),
                'permissions': permissions,
                'rootNode': {
                    'id': str(node['_id']),
                    'parentPath': node['parentPath'],
                }
            }
        elif type in ['api-token', 'link']:
            document = {
                'type': type,
                'label': label,
                'permissions': permissions,
                'rootNode': {
                    'id': str(node['_id']),
                    'parentPath': node['parentPath'],
                },
                'token': secrets.token_hex()
            }
        else:
            return
        result = collection.insert_one(document)
        result = collection.find_one({
            '_id': result.inserted_id
        })
        return result

    @rpc.rpc_register
    async def users_update(*, user: dict):
        result = collection.find_one_and_update(
            {
                '_id': user['_id']
            },
            {
                "$set": user
            },
            return_document=pymongo.ReturnDocument.AFTER
        )
        return result

    @rpc.rpc_register
    async def users_get_by_username(*, username: str, backend: str):
        result = collection.find_one({
            'type': 'user',
            'backend': backend,
            'username': username
        })

        return result

    @rpc.rpc_register
    async def users_get_by_token(*, token: str):
        result = collection.find_one({
            'token': token
        })

        return result
        
    @rpc.rpc_register
    async def users_get_by_invitation(*, invitation_id: str):
        result = collection.find_one({
            'type': 'invitation',
            'username': invitation_id
        })
        if result is None:
            return None

        return result
        
    @rpc.rpc_register
    async def users_get_by_id(*, account_id: str):
        result = collection.find_one({
            '_id': bson.ObjectId(account_id)
        })
        if result is None:
            return None

        return result

    @rpc.rpc_register
    async def users_delete(account_id: str):
        result = collection.find_one_and_delete({
            '_id': bson.ObjectId(account_id)
        })
        return {
            'status': 'ok'
        }

    @rpc.rpc_register
    async def users_get_by_node(node, find_children=True):
        node_id = str(node["_id"])
        regex = re.compile(f'^{node["parentPath"]}/{node_id}')
        result = collection.find({
            'rootNode.id': node_id
        })

        result = list(result)

        if find_children:
            regex = re.compile(f'^{node["parentPath"]}/{node_id}')
            result.extend(collection.find({
                'rootNode.parentPath': regex
            }))

        return list(result)

    await rpc.rpc_init()

    try:
        await asyncio.Future()
    except:
        pass

    await connection.close()

asyncio.run(main())